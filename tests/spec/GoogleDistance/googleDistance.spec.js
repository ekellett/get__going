describe('Google Distance Service Data', function(){
    var distanceData = {
              "destination_addresses" : [ "Grand Canal Dock, Dublin, Ireland" ],
               "origin_addresses" : [ "S Lotts Rd, Dublin, Co. Dublin, Ireland" ],
               "rows" : [
                  {
                     "elements" : [
                        {
                           "distance" : {
                              "text" : "0.6 km",
                              "value" : 609
                           },
                           "duration" : {
                              "text" : "3 mins",
                              "value" : 161
                           },
                           "status" : "OK"
                        }
                     ]
                  }
               ],
               "status" : "OK"
    }
    
    beforeEach(module('googleDistanceAPI'));
    
    beforeEach(inject(function(_googleDistanceAPI_) {
          googleDistanceAPI = _googleDistanceAPI_; 
    }));
        
    it('should return Google Distance data', function() {
       googleDistanceAPI.getData();
       expect(googleDistanceAPI.getData()).toEqual(distanceData);
    });
    
});