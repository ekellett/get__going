angular.module('googleDistanceAPI', [])
    .factory('googleDistanceAPI', function(){
        var service = {};
        
        service.getData = function(){
            return {
                       "destination_addresses" : [ "Grand Canal Dock, Dublin, Ireland" ],
                       "origin_addresses" : [ "S Lotts Rd, Dublin, Co. Dublin, Ireland" ],
                       "rows" : [
                          {
                             "elements" : [
                                {
                                   "distance" : {
                                      "text" : "0.6 km",
                                      "value" : 609
                                   },
                                   "duration" : {
                                      "text" : "3 mins",
                                      "value" : 161
                                   },
                                   "status" : "OK"
                                }
                             ]
                          }
                       ],
                       "status" : "OK"
                    }
                  }

         return service;
    });