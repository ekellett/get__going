angular.module('stationDetailsAPI', [])
    .factory('stationDetailsAPI', function(){
        var service = {};
        
        service.getData = function(){
            return {
                      "ArrayOfObjStationData": {
                        "-xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                        "-xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
                        "-xmlns": "http://api.irishrail.ie/realtime/",
                        "objStationData": [
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E221",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Howth",
                            "Destination": "Bray",
                            "Origintime": "16:15",
                            "Destinationtime": "17:26",
                            "Status": "No Information",
                            "Duein": "6",
                            "Late": "0",
                            "Exparrival": "16:20",
                            "Expdepart": "16:20",
                            "Scharrival": "16:20",
                            "Schdepart": "16:20",
                            "Direction": "To Bray",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E222",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Howth",
                            "Destination": "Bray",
                            "Origintime": "16:45",
                            "Destinationtime": "17:54",
                            "Status": "No Information",
                            "Duein": "36",
                            "Late": "0",
                            "Exparrival": "16:50",
                            "Expdepart": "16:50",
                            "Scharrival": "16:50",
                            "Schdepart": "16:50",
                            "Direction": "To Bray",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E223",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Howth",
                            "Destination": "Bray",
                            "Origintime": "17:15",
                            "Destinationtime": "18:24",
                            "Status": "No Information",
                            "Duein": "66",
                            "Late": "0",
                            "Exparrival": "17:20",
                            "Expdepart": "17:20",
                            "Scharrival": "17:20",
                            "Schdepart": "17:20",
                            "Direction": "To Bray",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E922",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Bray",
                            "Destination": "Howth",
                            "Origintime": "15:25",
                            "Destinationtime": "16:33",
                            "Status": "En Route",
                            "Lastlocation": "Departed Tara Street",
                            "Duein": "19",
                            "Late": "6",
                            "Exparrival": "16:33",
                            "Expdepart": "16:33",
                            "Scharrival": "16:26",
                            "Schdepart": "16:27",
                            "Direction": "To Howth",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E923",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Bray",
                            "Destination": "Howth",
                            "Origintime": "15:55",
                            "Destinationtime": "17:03",
                            "Status": "En Route",
                            "Lastlocation": "Departed Sandycove",
                            "Duein": "45",
                            "Late": "2",
                            "Exparrival": "16:58",
                            "Expdepart": "16:59",
                            "Scharrival": "16:56",
                            "Schdepart": "16:57",
                            "Direction": "To Howth",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E940",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Dun Laoghaire",
                            "Destination": "Howth",
                            "Origintime": "16:37",
                            "Destinationtime": "17:26",
                            "Status": "No Information",
                            "Duein": "65",
                            "Late": "0",
                            "Exparrival": "17:19",
                            "Expdepart": "17:19",
                            "Scharrival": "17:19",
                            "Schdepart": "17:19",
                            "Direction": "To Howth",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          },
                          {
                            "Servertime": "2016-12-01T16:14:14.643",
                            "Traincode": "E924",
                            "Stationfullname": "Bayside",
                            "Stationcode": "BYSDE",
                            "Querytime": "16:14:14",
                            "Traindate": "01 Dec 2016",
                            "Origin": "Bray",
                            "Destination": "Howth",
                            "Origintime": "16:27",
                            "Destinationtime": "17:35",
                            "Status": "No Information",
                            "Duein": "75",
                            "Late": "0",
                            "Exparrival": "17:29",
                            "Expdepart": "17:29",
                            "Scharrival": "17:29",
                            "Schdepart": "17:29",
                            "Direction": "To Howth",
                            "Traintype": "DART",
                            "Locationtype": "S"
                          }
                        ]
                      }
                    }
                }

         return service;
    });