//create IIFE to prevent global variables
(function () {

	//instatiate controller with dependency injections
	angular.module('app')
		.controller('GetAllStationsCtrl', ['$scope' , '$http', 'xmlParser', GetAllStationsCtrl]);
	
	function GetAllStationsCtrl($scope, $http, xmlParser){
		$scope.errorMsg = false;

		$scope.config = {
			baseUrl: 'http://cors.io/?http://api.irishrail.ie/realtime/realtime.asmx/',
			allStations: 'getAllStationsXML_WithStationType?StationType=D',
		}

		xmlParser.parseXML($scope.config.baseUrl+$scope.config.allStations)
			//create promise if successful
			.then(getDataSuccess, null)
			//catch error if unsuccessful
			.catch(errorCallback);

			//return data if successful
			function getDataSuccess(data){
				$scope.stationsData = data;
				$scope.allStations = $scope.stationsData.ArrayOfObjStation.objStation;
			}

			//log error if unsuccessful
			function errorCallback(errorMsg){
				$scope.errorMsg = true;
				console.log(errorMsg);
			}

	}

}());
