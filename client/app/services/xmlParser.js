(function () {
	'use strict';
	angular.module('app').factory('xmlParser',['$q', '$http', xmlParser]);

	function xmlParser($q, $http){
		return {
			parseXML: parseXML
		};

		function parseXML(url, data){
			return $http({
				method: 'GET',
				url: url,
				transformResponse:function(data) {
	                //convert the data to JSON and provide it to the success function below
	                var x2js = new X2JS();
	                var json = x2js.xml_str2json(data);
	                return json;
	            }
			})
			//pass functions to execute
			.then(sendResponseData)
			.catch(sendError)
		}

		//http response object passed as parameter to success function
		function sendResponseData(response) {
			//contains json returned from server
			return response.data;
		}

		//handles rejected http promise
		function sendError(response) {
			//return rejected promise with error message with http status code
			return $q.reject('Error ' + response.status);
		}
	}

}());