//create IIFE to prevent global variables
(function () {

	//instatiate controller with dependency injection
	angular.module('app')
	.controller('AppCtrl', ['$scope' , '$mdSidenav', '$timeout', '$mdDialog', '$location', AppCtrl]);	
	
	function AppCtrl($scope, $mdSidenav, $timeout, $mdDialog, $location){

		//get reminders from local storage
		$scope.savedReminders = localStorage.getItem('serviceDetails');
		//if there are no reminders parse json or set to empty array
		$scope.reminders = (localStorage.getItem('serviceDetails')!==null) ? JSON.parse($scope.savedReminders) : [];
		//convert to a JSON string
		localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
		
		//sidenav toggle
		$scope.openLeftMenu = function() {
			$mdSidenav('left').toggle();
		};

		$scope.closeLeftMenu = function() {
			close();
		};
		
		$scope.closeMenuSet = function() {
			$location.url('/');
			close();
		};
		
		$scope.closeMenuView = function() {
			$location.url('/reminders');
			close();
		};
		
		function close(){
			$mdSidenav('left').toggle();
		}

		//get the time now
		$scope.now = new Date();
		//convert times to milliseconds
		$scope.now = Date.parse($scope.now);

		//create reminder alert
		angular.forEach($scope.reminders, function(reminder, index) {
			$scope.departureDate = reminder.departureDate;
			$scope.reminderTime = reminder.reminderTime;
			//convert times to date
			$scope.reminderDate = new Date($scope.departureDate+" "+$scope.reminderTime);
			//convert to milliseconds
			$scope.reminderDate = Date.parse($scope.reminderDate);
			$scope.timeoutLength = $scope.reminderDate - $scope.now;
			//set a timeout for length of timeout
			//if time has not passed
			if($scope.timeoutLength > 0){
				$timeout(function () {
					//alert user
					reminderAlert();
				}, $scope.timeoutLength);
			}
		});

		function reminderAlert (ev){
			$mdDialog.show(
				$mdDialog.alert()
					.parent(angular.element(document.querySelector('body')))
					.clickOutsideToClose(true)
					.title('Time to go!')
					.textContent('You need to leave NOW to reach your train!')
					.ariaLabel('You need to leave NOW to reach your train!')
					.ok('Ok!')
					.targetEvent(ev)
				);
			}
		}

	}());