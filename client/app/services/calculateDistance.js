(function () {
	'use strict';
	angular.module('app').factory('calculateDistanceFactory',['$q', '$http', calculateDistanceFactory]);

	function calculateDistanceFactory($q, $http){
		return {
			calculateDistance: calculateDistance
		};

		function calculateDistance(url, data){
			return $http({
				method: 'GET',
				url: url
			})
			//pass functions to execute
			.then(sendResponseData)
			.catch(sendError)
		}

		//http response object passed as parameter to success function
		function sendResponseData(response) {
			//contains json returned from server
			return response.data;
		}

		//handles rejected http promise
		function sendError(response) {
			//return rejected promise with error message with http status code
			return $q.reject('Error ' + response.status);
		}
	}

}());