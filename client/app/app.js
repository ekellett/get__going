'use strict';

/**
 * @ngdoc overview
 * @name 
 * @description
 * 
 *
 * Main module of the application.
 */
var app = angular.module('app', ['ngRoute', 'ngMaterial']);

app.config(['$routeProvider', '$mdThemingProvider', function ($routeProvider, $mdThemingProvider) {
		$routeProvider
			.when('/', {
			    templateUrl: 'app/getAllStations/getAllStations-view.html',
			    controller: 'GetAllStationsCtrl'
			})
			.when('/add-reminder', {
			  templateUrl: 'app/addReminder/addReminder-view.html',
			  controller: 'AddReminderCtrl'
			})
			.when('/reminders', {
			  templateUrl: 'app/reminders/reminders-view.html',
			  controller: 'ViewReminderCtrl'
			})
			.when('/:station', {
			    templateUrl: 'app/stationDetails/stationDetails-view.html',
			    controller: 'StationDetailsCtrl'
			})	
			.otherwise({
			    redirectTo: '/'
			});

		$mdThemingProvider.theme('default')
			.primaryPalette('green')
			.accentPalette('light-green');
		}
]);
		
