//create IIFE to prevent global variables
(function () {

	//instatiate controller with dependency injections
	angular.module('app')
		.controller('StationDetailsCtrl', ['$scope','$http','$location','xmlParser','$log',StationDetailsCtrl]);
	
	function StationDetailsCtrl($scope, $http, $location, xmlParser, $log){
		$scope.errorMsg = false;
		//get station name from URL
		$scope.selectedStation = $location.url().substring(1);
		//get reminders from local storage
		$scope.savedReminders = localStorage.getItem('serviceDetails');
		//if there are no reminders parse json or set to empty array
		$scope.reminders = (localStorage.getItem('serviceDetails')!==null) ? JSON.parse($scope.savedReminders) : [];
		//convert to a JSON string
		localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
		

		$scope.config = {
			baseUrl: 'http://cors.io/?http://api.irishrail.ie/realtime/realtime.asmx/',
			stationDetails: 'getStationDataByNameXML?StationDesc=',
			selectedStation: $scope.selectedStation
		}
		
		xmlParser.parseXML($scope.config.baseUrl+$scope.config.stationDetails+$scope.config.selectedStation)
			//create promise if successful
			.then(getDataSuccess, null)
			//catch error if unsuccessful
			.catch(errorCallback);

			//return data if successful
			function getDataSuccess(data){
				$scope.stationDetails = data;
				$scope.currentStation = $scope.stationDetails.ArrayOfObjStationData.objStationData;
			}

			//log error if unsuccessful
			function errorCallback(errorMsg){
				$scope.errorMsg = true;
				console.log(errorMsg);
			}

			//add station details to reminder array
			$scope.getStationDetails = function(stationDeparture, stationDestination, departureTime, departureDate){
				$scope.reminders.push({
					stationDeparture:stationDeparture,
					stationDestination:stationDestination,
					departureTime:departureTime,
					departureDate: departureDate
				});	
				//convert to JSON string
				localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
				//redirect to add reminder page
				$location.url('/add-reminder');
				
			}
	}

}());