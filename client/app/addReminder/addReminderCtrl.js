//create IIFE to prevent global variables
(function () {

	//instatiate controller with dependency injections
	angular.module('app')
	.controller('AddReminderCtrl', ['$scope','$http','$log', 'calculateDistanceFactory', '$timeout', '$mdDialog', AddReminderCtrl]);
	
	function AddReminderCtrl($scope, $http, $log, calculateDistanceFactory, $timeout,$mdDialog){
		$scope.errorMsg = false;
		$scope.reminderSet = false;
		$scope.tooLate = false;
		
		//get reminders from local storage
		$scope.savedReminders = localStorage.getItem('serviceDetails');
		//if there are no reminders parse json or set to empty array
		$scope.reminders = (localStorage.getItem('serviceDetails')!==null) ? JSON.parse($scope.savedReminders) : [];
		//convert to a JSON string
		localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
		//get the last object 
		$scope.latestReminder = $scope.reminders.length-1;


		$scope.calculateDistance = function(){
				$scope.config = {
				baseUrl: 'http://cors.io/?https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial',
					params: {
						origin: $scope.reminder.origin,
						destinations: $scope.reminders[$scope.latestReminder].stationDeparture,
						mode: $scope.reminder.modeTransport,
						apiKey: 'AIzaSyDJx5HPR6ysg-QyfHw-SyXzee2S59VzYzo'
					}
				}

				calculateDistanceFactory.calculateDistance($scope.config.baseUrl+"&origins="+
					$scope.config.params.origin+"&destinations="+$scope.config.params.destinations+
					" train station, Co. Dublin&mode="+$scope.config.params.mode+"&key="+$scope.config.params.apiKey)
				//create promise if successful
				.then(getDataSuccess, null)
				//catch error if unsuccessful
				.catch(errorCallback);

				//return data if successful
				function getDataSuccess(data){
					$scope.response = data;
					//get duration from Google Distance API
					$scope.duration = $scope.response.rows[0].elements[0].duration.text;
					//call the getAlert function
					getAlert();
				}

				//log error if unsuccessful
				function errorCallback(errorMsg){
					$scope.errorMsg = true;
					console.log(errorMsg);
				}
			}

			function getAlert(){
				//split text between space to array
				$scope.durationResponse = $scope.duration;
				$scope.duration = $scope.duration.split(" ");
				//split text between : to array
				$scope.departureTime = $scope.reminders[$scope.latestReminder].departureTime.split(":");			
				//if duration response contains hours
				if($scope.durationResponse.length > 7) {
					$scope.durationHour = parseInt($scope.duration[0]);
					$scope.durationMins = parseInt($scope.duration[2]);
					calculateMins();
					//subtract duration hours from current alert time
					$scope.reminderH = $scope.reminderH - $scope.durationHour;
					//create reminder time string
					$scope.reminderTime = $scope.reminderH+":"+$scope.reminderM;
				} else {
					//convert first element to number
					$scope.durationMins = parseInt($scope.duration[0]);
					calculateMins();	
					$scope.reminderTime = $scope.reminderH+":"+$scope.reminderM;
				}
				//add reminder time to object
				$scope.reminders[$scope.latestReminder].reminderTime = $scope.reminderTime;
				localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));

				//get the time now
				$scope.now = new Date();
				//convert alert time to date
				$scope.reminderDate = new Date($scope.reminders[$scope.latestReminder].departureDate+" "+$scope.reminderTime);
				//convert times to milliseconds
				$scope.now = Date.parse($scope.now);
				$scope.reminderMS = Date.parse($scope.reminderDate);
				//if the time now is greater or equal to the alert time it is too late to reach train
				if($scope.now >= $scope.reminderMS){
					$scope.reminderSet = false;
					$scope.tooLate = true;
					tooLateAlert();
					//remove object from local Storage
					$scope.reminders.splice($scope.latestReminder, 1);
					localStorage.removeItem($scope.latestReminder);
				    //convert to JSON string
				    localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
				} else {
					$scope.reminderSet = true;
					$scope.tooLate = false;
					reminderSetAlert();
				}
	
			}

			function calculateMins(){				
				//separate mins and convert to an integer
				$scope.departureM = parseInt($scope.departureTime[1]);
				//subtract duration from scheduled time;
				$scope.reminderM = parseInt($scope.departureM - $scope.durationMins);
				$scope.reminderH = parseInt($scope.departureTime[0]);
				//if the subtraction results in a negative number
					if($scope.reminderM < 0){
						//subtract one from the hour
						$scope.reminderH = parseInt($scope.departureTime[0]) -1;
						//convert to positive number and subtract remainder from mins 
						$scope.reminderM = 60 - Math.abs($scope.reminderM);
						//if less than 10 add 0 before
					} else if ($scope.reminderM > 0 &&  $scope.reminderM < 10){
						$scope.reminderM = "0"+$scope.reminderM;
					}
			}

			function reminderSetAlert (ev){
				$mdDialog.show({
			      contentElement: '#reminderSet',
			      parent: angular.element(document.body),
			      targetEvent: ev,
			      clickOutsideToClose: true
			    })
			}

			function tooLateAlert (ev) {
               $mdDialog.show({
			      contentElement: '#reminderSet',
			      parent: angular.element(document.body),
			      targetEvent: ev,
			      clickOutsideToClose: true
			    });
            }
	}

}());

