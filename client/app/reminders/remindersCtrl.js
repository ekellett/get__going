//create IIFE to prevent global variables
(function () {

	//instatiate controller with dependency injections
	angular.module('app')
	.controller('ViewReminderCtrl', ['$scope','$http','$log', 'calculateDistanceFactory', '$timeout', '$mdDialog', ViewReminderCtrl]);
	
	function ViewReminderCtrl($scope, $http, $log, calculateDistanceFactory, $timeout,$mdDialog){

		//get reminders from local storage
		$scope.savedReminders = localStorage.getItem('serviceDetails');
		//if there are no reminders parse json or set to empty array
		$scope.reminders = (localStorage.getItem('serviceDetails')!==null) ? JSON.parse($scope.savedReminders) : [];
		//convert to a JSON string
		localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
		
		//get the time now
		$scope.now = new Date();
		//loop through reminders
		angular.forEach($scope.reminders, function(reminder, index) {
			$scope.reminderTime = reminder.reminderTime;
			//convert alert time to date
			$scope.reminderDate = new Date(reminder.departureDate+" "+$scope.reminderTime);
			//convert times to milliseconds
			$scope.now = Date.parse($scope.now);
			$scope.reminderDate= Date.parse($scope.reminderDate);
			//delete reminder if it is in the past or reminder time is empty
			if ($scope.reminderDate < $scope.now || $scope.reminderTime == null) {
				//remove object from array
				$scope.reminders.splice(index,1);
				//convert to a JSON string
				localStorage.setItem('serviceDetails', JSON.stringify($scope.reminders));
			  }
		});

	}

}());

