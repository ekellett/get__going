# Prerequisites

1. [NodeJS]


# Launching the project

With Node.js installed, cd into the project root folder from the command line and run *npm install* to install dependencies. Run *node server.js* to launch the application over http://localhost:3000/#/


# Running unit tests

Again cd into the project root folder and from the command line run *npm test*